# -*- coding: utf-8 -*-
"""
Created on Tue Sep 22 20:41:29 2020

@author: LENOVO
"""
"""
import cv2

img = cv2.imread('lena.png')

# coco names i okuyoruz(dahil ediyoruz)
classNames = []
classFile = 'coco.names'
with open(classFile,'rt') as f:
    classNames = f.read().rstrip('\n').split('\n')

print(classNames)

configPath = 'ssd_mobilenet_v3_large_coco_2020_01_14.pbtxt'
weightsPath = 'frozen_inference_graph.pb'

net = cv2.dnn_DetectionModel(weightsPath,configPath)
net.setInputSize(320,320)
net.setInputScale(1.0/127.5)
net.setInputMean((127.5,127.5,127.5))
net.setInputSwapRB(True)

#boundingbox(bbox) -> rectangle çizmemizi sağlar
#classIds ise -> tespit edilen objenin ismini yazmamızı sağlar
classIds, confs, bbox = net.detect(img,confThreshold=float(0.5))
print(classIds,bbox)


cv2.imshow("output",img)
cv2.waitKey(0)
"""

import cv2
import numpy as np
#img =cv2.imread('WhatsApp Image 2020-09-25 at 20.05.57.jpeg')
cap = cv2.VideoCapture(0)

whT = 320
confTheresholds = 0.5
nmsThereshold = 0.3

classesFile = 'coco.names'
classNames = []
with open(classesFile,'rt') as f:
    classNames = f.read().rstrip('\n').split('\n')
    

#print(classNames)    
#print(len(classNames))    


modelConfig = 'yolov3-tiny.cfg'
modelWeights = 'yolov3-tiny.weights'

net = cv2.dnn.readNetFromDarknet(modelConfig,modelWeights)
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)

def findObjects(outputs,img):
    hT,wT,cT = img.shape
    bbox = []  
    classIds = []
    confs = []
    for outputs in outputs:
        for det in outputs:
            scores = det[5:]
            classId = np.argmax(scores)
            confidence = scores[classId]
            if confidence > confTheresholds:
                w,h = int(det[2]*wT) ,int(det[3]*hT)
                x,y = int((det[0]*wT)-w/2) , int((det[1]*hT) - h/2)
                bbox.append([x,y,w,h])
                classIds.append(classId)
                confs.append(float(confidence))            
    print(len(bbox))           
    indices = cv2.dnn.NMSBoxes(bbox,confs,confTheresholds,nmsThereshold) 
    print(indices)
    
    for i in indices:
        i = i[0]
        box = bbox[i]
        x,y,w,h = box[0],box[1],box[2],box[3]
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)
        cv2.putText(img,f'{classNames[classIds[i]].upper() } {int(confs[i]*100)}%',
                           (x,y-10),cv2.FONT_HERSHEY_SIMPLEX,0.6,(0,0,255),2)
        
           

while True:
    ret , img = cap.read()
    
    blob = cv2.dnn.blobFromImage(img,1/255,(whT,whT),[0,0,0],1,crop=False)
    net.setInput(blob)
    
    layerNames = net.getLayerNames()
#    print(layerNames)
    
    outputNames = [layerNames[i[0]-1] for i in net.getUnconnectedOutLayers()]
#    print(net.getUnconnectedOutLayers())
    
    outputs = net.forward(outputNames)
    print(outputs[0].shape)
    print(outputs[1].shape)
#    print(outputs[2].shape)
    
    findObjects(outputs,img)
    
    
    cv2.imshow('output',img)
    cv2.waitKey(1)
    if cv2.waitKey(1) & 0xFF == ord('Q'):
        break

cap.release()
cv2.destroyAllWindows()     






























